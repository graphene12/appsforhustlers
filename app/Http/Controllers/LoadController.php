<?php

namespace App\Http\Controllers;
use App\Repositories\User\UserRepository;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Str;
use Auth;
use Session;
use App\Models\Userlog;
use Jenssegers\Agent\Agent;
use Hash;
use Illuminate\Support\Facades\Mail;
use App\Mail\Appreciation;

class LoadController extends Controller
{
    protected $user;

    public function __construct(UserRepository $user){
        $this->user = $user;
    }

    public function validateEmail(array $data){
        return Validator::make($data,[
            'email' => 'required|email|unique:users,email'
        ]);
    }

    public function validateData(array $data){
        return Validator::make($data,[
            'ccustname' => 'required|string',
            'ccustemail' => 'required|email|unique:users,email',
            'cproditem' => 'required|integer'
        ]);
    }

    public function registration(Request $request){

        $data = $request->toArray();

        $validation = $this->validateData($data);

        if($validation->fails()){
            return response()->json($validation->errors());
        }else{
            $password = Str::random(10);

         $userdata = [
        'name' => ucwords(strtolower(trim($request['ccustname']))),
        'email' => $request['ccustemail'],
        'password' => Hash::make($password),
        ];
            if($data['cproditem'] == '336585'){
                $success = $this->user->create($userdata);

                if($success){
                    $this->sendFirstEmail($userdata['email'],$password);
                    return response()->json([
                        'message' => 'Registration Successful. This Login Credentials will be sent to your email address. ',
                        'Login Credentials' => [
                            'email' => $userdata['email'],
                            'password' => $password
                        ]
                    ]);

                }else{
                    return response()->json([
                        'message' => 'Registration not Successful'
                    ]);

                }
            }else{
                return response()->json(['Invalid Product']);
            }



        }

    }

    public function sendFirstEmail($email,$password){

        return Mail::to($email)->send(new Appreciation($email,$password));
    }

    public function validateLogin(array $data){
        return Validator::make($data,[
            'email' => 'required|exists:users,email|email',
            'password' => 'required|string'
        ]);
    }

    public function saveLoginInfo(Request $request){
        $agent = new Agent();
        $data['operating_system'] = $agent->platform()." ".$agent->version($agent->platform());
        $data['browser'] = $agent->browser()." ".$agent->version($agent->browser());
        $data['ip_address'] = $request->ip();
        $data['user_id'] = Auth::user()->id;
        UserLog::create($data);
    }

    public function authentication(Request $request){
        $validation = $this->validateLogin($request->except('_token'));

        if($validation->fails()){
            return redirect()->back()->withErrors($validation->errors())->withInput();
        }else{
            if(Auth::attempt(['email' => $request['email'], 'password' => $request['password'],'role' => '1','active' => '0'],1)){
                return redirect()->back()->withErrors(['password' => "Your Account has been Suspended, Please Contact Support."])->withInput();
            }else if(Auth::attempt(['email' => $request['email'], 'password' => $request['password'],'role' => '1','active' => '1'],1)){
                $this->saveLoginInfo($request);
                return redirect()->route('user_home');
            }else if(Auth::attempt(['email' => $request['email'], 'password' => $request['password'],'role' => '0','active' => '1'],1)){
                $this->saveLoginInfo($request);
                return redirect()->route('admin_home');
            }else {
                return redirect()->back()->withErrors(['password' => "Your password is Invalid"])->withInput();
            }
        }
    }


}
