<?php

namespace App\Http\Controllers\User;
use Auth;
use Session;
use App\Repositories\User\UserRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Mail;
use App\Mail\SupportDesk;
use Hash;
use App\Mail\Appreciation;

class LoadController extends Controller
{
    protected $user;

    public function __construct(UserRepository $user){
        $this->user = $user;
    }

    public function logout(Request $request){
        Auth::logout();
        return redirect()->route('login_page')->withErrors(['logout' => 'You have been Successfully Logged out.']);
    }

    public function validateFunction(array $data,array $rules,$messages = array()){
        return Validator::make($data,$rules,$messages);
    }

    public function updateProfile(Request $request){
        $data = $request->except('_token');
        $rules = [
            'name' => 'required|string',
            'password' => 'nullable|required_with:confirm_password|string|min:8',
            'confirm_password' => 'nullable|required_with:password|same:password',
        ];

        $validation = $this->validateFunction($data,$rules);

            if($validation->fails()){
                return redirect()->back()->withErrors($validation->errors())->withInput();
            }else{
                $success = $this->user->update($data);
                Session::put('green',1);
                return redirect()->back()->withErrors(['You have Successfully updated your profile']);

            }
    }

    public function createUser(Request $request){
        $data = $request->except('_token');
        $rules = [
            'name' => 'required|string',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|required_with:confirm_password|string|min:8',
            'confirm_password' => 'required|required_with:password|same:password',
            'role' => 'required|integer',
        ];

        $validation = $this->validateFunction($data,$rules);
            if($validation->fails()){
                return redirect()->back()->withErrors($validation->errors())->withInput();
            }else{
                unset($data['confirm_password']);
                $data['password'] = Hash::make($data['password']);
                $data['name'] = ucwords(strtolower($data['name']));
                $success = $this->user->create($data);

                Mail::to($data['email'])->send(new Appreciation($data['email'],$data['password']));

                Session::put('green',1);
                return redirect()->back()->withErrors(['You have Successfully created new User']);
            }
    }

    public function sendMail(Request $request){

        $message = $request['message'];

        $success = Mail::to("support@appsforhustlers.com")->send(new SupportDesk($message));

        Session::put('green',1);
        return redirect()->back()->withErrors("Your Email has been sent. We'll get back to you within the next 6 hours");

    }

}
