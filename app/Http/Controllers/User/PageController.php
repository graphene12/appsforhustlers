<?php

namespace App\Http\Controllers\User;
use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
class PageController extends Controller
{
    public function home(Request $request){
        $title = "User - Home";
        return view('user.home',['title'=>$title]);
    }

    public function adminHome(Request $request){
        $title = "Admin - Home";
        $users = User::where('role','1')->count();
        return view('user.admin.home',['title'=>$title,'users_count'=>$users]);
    }

    public function profile(Request $request){
        $title = "Profile Page";
        return view('user.profile',['title' => $title]);
    }

    public function createUser(Request $request){
        $title = "Create User";
        return view('user.admin.new_user',['title'=>$title]);
    }

    public function allUsers(Request $request){
        $title = "All Users";
        $users = User::where('role','1')->get();
        return view('user.admin.users',['title' => $title,'users' => $users]);
    }

    public function displayLoginInfo(Request $request,$id){
        $user = User::find($id);
        $title = "User Login Information";
        return view('user.admin.login_info',['user'=>$user,'title' => $title]);
    }

    public function changeUserStatus(Request $request,$id){
        $user = User::find($id);
        if($user->active == '0'){
            $user->active = '1';
            $user->save();
            $message = ['User has been Activated Successfully'];
        }else{
            $user->active = '0';
            $user->save();
            $message = ['User has been Deactivated Successfully'];

        }

        Session::put('green',1);
        return redirect()->route('all_users')->withErrors($message);
    }
}
