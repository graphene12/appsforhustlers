<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class User
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::guard('user')->check()){
            return redirect()->route('login_page')->withErrors(['password' => "You're not Authorised to access this page, Please Login."]);
        }
        return $next($request);
    }
}
