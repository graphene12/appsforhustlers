<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Userlog extends Model
{
    protected $table = "user_log";
    
    protected $fillable = [
        'user_id', 'ip_address', 'operating_system', 'browser'
    ];
}
