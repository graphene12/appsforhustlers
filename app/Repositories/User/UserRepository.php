<?php

namespace App\Repositories\User;
use Illuminate\Http\Request;
use App\Models\User;
use Hash;
use Auth;

class UserRepository implements UserInterface{

    public function create(array $data){
        return User::create($data);
    }

    public function update(array $data){
        $user = User::where('email',Auth::user()->email)->first();

        if(empty($data['password'])){
            return User::where('email',Auth::user()->email)->update(['name' => $data['name']]);
        }else{
            return User::where('email',Auth::user()->email)->update(['name' => $data['name'],'password' => Hash::make($data['password'])]);
        }
    }
}
