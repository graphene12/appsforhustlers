<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Inim Andrew',
            'email' => 'grapheneee@gmail.com',
            'password' => Hash::make('polinium'),
            'role' => '0',
            ]);
    }
}
