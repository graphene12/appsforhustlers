@if (Session::has('green') | Session::has('red') | Session::has('green'))

<section class="mbr-parallax-background mbr-after-navbar" style="background-position: 50% 50%;
background-repeat: no-repeat;background-size: cover;padding-top:1.4em;" id="msg-box1-4" data-rv-view="181" >
    <div class="mbr-overlay" style="opacity: 0.8; "></div>

    <div class="container">
        <div class="row">

            <div>

                <div class="col-sm-12">

                        @foreach($errors->all() as $error)

                        @if(Session::has('green'))
                        <div class ="alert alert-success alert-dismissable" >
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <center style="color:black;">{{$error}}</center>
                        </div>
                        @elseif(Session::has('red'))
                        <div class ="alert alert-danger alert-dismissable" >
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <center style="color:black;">{{$error}}</center>
                        </div>
                        @endif
                    @endforeach
                    <?php Session::forget('red'); Session::forget('green');  ?>
                </div>

            </div>
        </div>
    </div>
</section>
@endif
