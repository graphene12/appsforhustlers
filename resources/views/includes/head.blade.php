<!DOCTYPE html>
<html  >
<head>
  <!-- Site made with Mobirise Website Builder v4.11.6, https://mobirise.com -->
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="generator" content="Mobirise v4.11.6, mobirise.com">
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
  <link rel="shortcut icon" href="{{url('mobirise/assets/images/3-113x128.png')}}" type="image/x-icon">
  <meta name="description" content="">

  <title>{{$title}}</title>
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700&subset=latin,cyrillic">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Alegreya+Sans:400,700&subset=latin,vietnamese,latin-ext">
  <link rel="stylesheet" href="{{url('mobirise/assets/web/assets/mobirise-icons/mobirise-icons.css')}}">
  <link rel="stylesheet" href="{{url('mobirise/assets/tether/tether.min.css')}}">
  <link rel="stylesheet" href="{{url('mobirise/assets/bootstrap/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{url('mobirise/assets/puritym/css/style.css')}}">
  <link rel="stylesheet" href="{{url('mobirise/assets/dropdown-menu/style.light.css')}}">
  <link rel="preload" as="style" href="{{url('mobirise/assets/mobirise/css/mbr-additional.css')}}">
  <link rel="stylesheet" href="{{url('mobirise/assets/mobirise/css/mbr-additional.css')}}" type="text/css">
  <style>

  .error{
      color: red;
  }

    .head{
        color: black;
        font-weight: bold;
        padding-top:20px;
        padding-left: 20px;
        text-transform:uppercase;
        font-family: 'Rubik', sans-serif;
    }
    .text-xs-right{
        font-weight: bold;
        font-size: 34px;
    }
    .new-item{
        padding:10px;
        font-weight: bold;
    }
    a{
        color: black;
    }

</style>

</head>
