@include('includes.head')
<body>
@include('includes.navbar')
@if(count($errors) > 0)
                    @include('includes.errors')
                @endif

                <section class="mbr-section mbr-section-small mbr-parallax-background mbr-after-navbar" id="msg-box1-4" data-rv-view="181" style="background-image: url('{{url('mobirise/assets/images/mbr-1-1920x1281.jpg')}}' ); padding-top: 6rem; padding-bottom: 6rem;">
                    <div class="mbr-overlay" style="opacity: 0.8; background-color: rgb(239, 83, 14);"></div>

                    <div class="container">
                        <div class="row" style="padding-top:3em;">

                            <div>
                                @if(Request::is('user/home'))
                                <div class="col-sm-8">
                                    <h2 class="mbr-section-title h1">Howdy, {{Auth::user()->name}}</h2>
                                    <p class="lead">Kindly Login To Your 8 Super Softwares Below!</p>
                                </div>
                                <div class="col-sm-4 text-xs-right" style="padding-top:1em;"><a class="btn btn-lg btn-white" href="#"><span class="mbri-home mbr-iconfont mbr-iconfont-btn"></span>Click Here to Watch Tutorials</a></div>
                                @elseif(Request::is('profile'))
                                <div class="col-sm-8">
                                    <h2 class="mbr-section-title h1">User Profile</h2>
                                </div>
                                <div class="col-sm-4 text-xs-right" style="padding-top:1em;"><a class="btn btn-lg btn-white" href="{{route('user_home')}}"><span class="mbri-home mbr-iconfont mbr-iconfont-btn"></span>Go back home</a></div>
                                @else
                                <div class="col-sm-8">
                                    <h2 class="mbr-section-title h1">{{$title}}</h2>
                                </div>
                                @if(Auth::user()->role == '0')
                                <div class="col-sm-4 text-xs-right" style="padding-top:1em;"><a class="btn btn-lg btn-white" href="{{route('admin_home')}}"><span class="mbri-home mbr-iconfont mbr-iconfont-btn"></span>Go back home</a></div>
                                @else
                                <div class="col-sm-4 text-xs-right" style="padding-top:1em;"><a class="btn btn-lg btn-white" href="{{route('user_home')}}"><span class="mbri-home mbr-iconfont mbr-iconfont-btn"></span>Go back home</a></div>
                                @endif
                                @endif

                            </div>
                        </div>
                    </div>
                </section>
@yield('content')

<div class="row">
    <div class="col-md-4">
        <div class="white-box">
                        <!-- sample modal content -->
            <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title" id="myLargeModalLabel">Support Desk</h4> </div>
                        <div class="modal-body">
                            <form action="{{route('send_email')}}" method="POST" id="sendMail">
                                <div class="form-group">
                                        <textarea required name="message" cols="30" rows="10"  class="form-control" placeholder="Type your Message Here..."></textarea>
                                </div>
                                {{ csrf_field() }}
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-success waves-effect text-left" form="sendMail" >Send</button>
                            <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
         </div>
    </div>

@include('includes.footer')
@include('includes.script')
</body>
</html>
