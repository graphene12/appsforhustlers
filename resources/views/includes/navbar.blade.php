<section id="dropdown-menu-0" data-rv-view="184">

        <nav class="navbar navbar-dropdown navbar-fixed-top">

            <div class="container">

                <div class="navbar-brand">
                    <a href="https://app.appsforhustlers.com" class="navbar-logo"><img src="{{url('mobirise/assets/images/3-113x128.png')}}" alt="AppsForHustlers"></a>
                    <a class="text-black" href="#"><strong>AppsForHustlers</strong></a>
                </div>

                <button class="navbar-toggler pull-xs-right hidden-md-up" type="button" data-toggle="collapse" data-target="#exCollapsingNavbar">
                    <div class="hamburger-icon"></div>
                </button>

                <ul class="nav-dropdown collapse pull-xs-right navbar-toggleable-sm nav navbar-nav" id="exCollapsingNavbar">
                    <li class="nav-item"><a class="nav-link link" href="#"></a></li>
                    @if (Auth::user()->role == '0')
                    <li class="nav-item nav-btn dropdown"><a class="nav-link btn btn-black-outline btn-black" href="{{route('admin_home')}}"><span class="mbri-features mbr-iconfont mbr-iconfont-btn"></span>Home</a></li>
                    <li class="dropdown nav-item nav-btn" >
                            <a class="dropdown-toggle nav-link btn btn-black-outline btn-black" data-toggle="dropdown" href="#"> User Mgt </a>
                            <ul class="dropdown-menu">
                                    <li class="nav-item new-item "><a href="{{route('create_user')}}">Create Users</a></li><br>
                                    <li class="nav-item new-item"><a href="{{route('all_users')}}">All Users</a></li>
                            </ul>
                            <!-- /.dropdown-user -->
                        </li>
                    @else
                    <li class="nav-item nav-btn dropdown"><a class="nav-link btn btn-black-outline btn-black" href="{{route('user_home')}}"><span class="mbri-features mbr-iconfont mbr-iconfont-btn"></span>Home</a></li>
                    @endif
                    <li class="nav-item nav-btn"><a class="nav-link btn btn-black-outline btn-black" href="#" data-toggle="modal" data-target=".bs-example-modal-lg" ><span class="mbri-more-horizontal mbr-iconfont mbr-iconfont-btn"></span>Support Desk</a></li>
                    <li class="nav-item nav-btn"><a class="nav-link btn btn-black-outline btn-black" href="{{route('profile_page')}}"><span class="mbri-user mbr-iconfont mbr-iconfont-btn"></span>Profile</a></li>
                    <li class="nav-item nav-btn"><a class="nav-link btn btn-black-outline btn-black" href="{{route('logout')}}"><span class="mbri-logout mbr-iconfont mbr-iconfont-btn"></span>Log Out</a></li>



                </ul>

            </div>

        </nav>

    </section>
