@extends('includes.master')

@section('content')


<section class="mbr-section mbr-section-small" id="features1-0" data-rv-view="186" style="background-color: rgb(255, 255, 255); padding-top: 4.5rem; padding-bottom: 3rem;">

    <div class="container">
        <div class="row">


            <div class="col-sm-6 col-md-4">
                <div class="card cart-block">
                    <h2 class="head">Total Users</h2>
                    <div class="card-block">
                            <div class="text-xs-right"><span >{{$users_count}}</span></div>
                    </div>
                </div>
            </div>


            <div class="col-sm-6 col-md-4">
                    <div class="card cart-block">
                            <h2 class="head">Total Earnings</h2>
                        <div class="card-block">

                                <div class="text-xs-right">$ {{$users_count * 97}} </div>
                        </div>
                    </div>
                </div>

        </div>

    </div>
</section>

@endsection
