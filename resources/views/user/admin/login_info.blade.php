@extends('includes.master')

@section('content')
<section class="mbr-section mbr-section-small mbr-parallax-background mbr-after-navbar" data-rv-view="181" >
    <div class="mbr-overlay" style="opacity: 0.8; "></div>

    <div class="container">
        <div class="row">
                <center><h4>Login Information for {{$user->name}}</h4></center>
            <div>

                <div class="col-sm-12">

                        <table class="table">
                            <thead class="thead-inverse">
                                <tr>
                                    <th>Date</th>
                                    <th>Browser</th>
                                    <th>Operating System</th>
                                    <th>Ip-Address</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($user->userlogs as $log)
                                <tr>
                                    <td>{{$log->created_at}}</td>
                                    <td>{{$log->browser}}</td>
                                    <td>{{$log->operating_system}}</td>
                                    <td>{{$log->ip_address}}</td>

                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                </div>

            </div>
        </div>
    </div>
</section>

@endsection
