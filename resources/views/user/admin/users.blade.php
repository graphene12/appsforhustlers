@extends('includes.master')

@section('content')
<section class="mbr-section mbr-section-small mbr-parallax-background mbr-after-navbar" data-rv-view="181" >
    <div class="mbr-overlay" style="opacity: 0.8; "></div>
<?php
    function status($status){
        if($status == '0'){
            return "Suspended";
        }else{
            return "Active";
        }
    }

?>
    <div class="container">
        <div class="row">
            <div>

                <div class="col-sm-12">

                        <table class="table">
                            <thead class="thead-inverse">
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Price</th>
                                    <th>Status</th>
                                    <th style="text-align:center;">Manage</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($users as $user)
                                <tr>
                                    <td>{{$user->name}}</td>
                                    <td>{{$user->email}}</td>
                                    <td>$97</td>
                                    <td>{{status($user->status)}}</td>
                                    <td>
                                        <a href="{{route('login_info',[$user->id])}}" class="btn btn-primary">Login Info</a>
                                        @if ($user->active == '0')
                                        <a href="{{route('change_status',[$user->id])}}" class="btn btn-success">Activate</a>
                                        @else
                                        <a href="{{route('change_status',[$user->id])}}" class="btn btn-warning">Deactivate</a>
                                        @endif

                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                </div>

            </div>
        </div>
    </div>
</section>

@endsection
