@extends('includes.master')

@section('content')

<section class="mbr-section mbr-section-small" id="features1-0" data-rv-view="186" style="background-color: rgb(255, 255, 255); padding-top: 4.5rem; padding-bottom: 3rem;">

    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-4">
                <div class="card cart-block">
                    <div><img src="{{url('mobirise/assets/images/whatis-1-600x384.png')}}" class="card-img-top"></div>
                    <div class="card-block">
                        <h4 class="card-title text-xs-center"><strong>PixieLogo (Local Edition)</strong></h4>

                        <div class="text-xs-center"><a href="https://pixielogo.com/app" class="btn btn-secondary-outline">Login Here</a></div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4">
                <div class="card cart-block">
                    <div><img src="{{url('mobirise/assets/images/screenshot-2-600x368.png')}}" class="card-img-top"></div>
                    <div class="card-block">
                        <h4 class="card-title text-xs-center"><strong>xDealy</strong></h4>

                        <div class="text-xs-center"><a href="http://my.xdealy.com/signup.php?token=DMaubykXkNdVuVP" class="btn btn-secondary-outline">Login Here</a>
                            <a href="http://my.xdealy.com/signup.php?token=unsrc0wl63lq7go" class="btn btn-secondary-outline">Register Here</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4">
                <div class="card cart-block">
                    <div><img src="{{url('mobirise/assets/images/images-600x408.jpeg')}}" class="card-img-top"></div>
                    <div class="card-block">
                        <h4 class="card-title text-xs-center"><strong>YouTargetr</strong></h4>

                        <div class="text-xs-center"><a href="https://members.youtargetr.com/" class="btn btn-secondary-outline">Login Here</a></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section class="mbr-section mbr-section-small" id="features1-1" data-rv-view="189" style="background-color: rgb(255, 255, 255); padding-top: 0rem; padding-bottom: 3rem;">

    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-4">
                <div class="card cart-block">
                    <div><img src="{{url('mobirise/assets/images/clab-prod-boxes-small.png')}}" class="card-img-top"></div>
                    <div class="card-block">
                        <h4 class="card-title text-xs-center"><strong>ContentLab</strong></h4>

                        <div class="text-xs-center"><a href="https://app.getcontentlab.com/login" class="btn btn-secondary-outline">Login Here</a></div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4">
                <div class="card cart-block">
                    <div><img src="{{url('mobirise/assets/images/images-2-600x388.jpg')}}" class="card-img-top"></div>
                    <div class="card-block">
                        <h4 class="card-title text-xs-center"><strong>Click Fomo</strong></h4>

                        <div class="text-xs-center"><a href="https://clickfomo.com/" class="btn btn-secondary-outline">Login Here</a></div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4">
                <div class="card cart-block">
                    <div><img src="{{url('mobirise/assets/images/ob-806752-dfy-hero-review-600x548.jpeg')}}" class="card-img-top"></div>
                    <div class="card-block">
                        <h4 class="card-title text-xs-center"><strong>DFY Hero</strong></h4>

                        <div class="text-xs-center"><a href="https://dfyhero.com/login" class="btn btn-secondary-outline">Login Here</a>
                            <a href="https://dfyhero.com/register/1/dfy-hero-agency-now" class="btn btn-secondary-outline">Register Here</a></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section class="mbr-section mbr-section-small" id="features1-2" data-rv-view="192" style="background-color: rgb(255, 255, 255); padding-top: 0rem; padding-bottom: 4.5rem;">

    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-4">
                <div class="card cart-block">
                    <div><img src="{{url('mobirise/assets/images/socicake-600x383.png')}}" class="card-img-top"></div>
                    <div class="card-block">
                        <h4 class="card-title text-xs-center"><strong>SociCake</strong></h4>

                        <div class="text-xs-center"><a href="https://account.socibundle.com/" class="btn btn-secondary-outline">Login Here</a></div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4">
                <div class="card cart-block">
                    <div><img src="{{url('mobirise/assets/images/designrig-box-mock-600x338.jpg')}}" class="card-img-top"></div>
                    <div class="card-block">
                        <h4 class="card-title text-xs-center"><strong>DesignRig</strong></h4>

                        <div class="text-xs-center"><a href="http://www.designrid.com/login" class="btn btn-secondary-outline">Login Here</a></div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4">
                <div class="card cart-block">
                    <div><span class="mbri-smile-face mbr-iconfont mbr-iconfont-features1"></span></div>
                    <div class="card-block">
                        <h4 class="card-title text-xs-center"><strong>Join Our FB Mastermind Circle</strong></h4>

                        <div class="text-xs-center"><a href="https://web.facebook.com/groups/appsforhustlers/" class="btn btn-secondary-outline">JoinHere</a></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
@endsection
