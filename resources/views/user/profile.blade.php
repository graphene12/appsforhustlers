@extends('includes.master')

@section('content')
<section class="mbr-section mbr-section-small mbr-parallax-background mbr-after-navbar" data-rv-view="181" >
    <div class="mbr-overlay" style="opacity: 0.8; "></div>

    <div class="container">
        <div class="row">
            <div>

                <div class="col-sm-12">

                        <form class="form-horizontal form-material" method="POST" action="{{route('update_profile')}}">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label class="col-md-12">Full Name</label>
                                <div class="col-md-12">
                                    <input type="text" name="name" placeholder="Johnathan Doe" class="form-control form-control-line" value="{{Auth::user()->name}}"> </div>
                                    <center class ="error">{{ $errors->first('name') }}</center>
                            </div>
                            <div class="form-group">
                                <label for="example-email" class="col-md-12">Email</label>
                                <div class="col-md-12">
                                    <input type="email" name="email" placeholder="johnathan@admin.com" class="form-control form-control-line" value="{{Auth::user()->email}}" disabled> </div>
                                    <center class ="error">{{ $errors->first('email') }}</center>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">New Password</label>
                                <div class="col-md-12">
                                    <input type="password" placeholder="Password" class="form-control form-control-line" name="password" value="{{old('password')}}" > </div>
                                    <center class ="error">{{ $errors->first('password') }}</center>
                            </div>
                            <div class="form-group" >
                                    <label class="col-md-12">Confirm Password</label>
                                    <div class="col-md-12">
                                        <input type="password" placeholder="Retype Password" name="confirm_password" class="form-control form-control-line" value="{{old('confirm_password')}}"> </div>
                                        <center class ="error">{{ $errors->first('confirm_password') }}</center>
                                </div>

                            <div class="form-group" >
                                <div class="col-sm-12">
                                    <button type="submit" class="btn btn-success" >Update Profile</button>
                                </div>
                            </div>
                        </form>
                </div>

            </div>
        </div>
    </div>
</section>

@endsection
