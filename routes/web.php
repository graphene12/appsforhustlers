<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@login')->name('login_page');

Route::get('logout','LoadController@logout')->name('logout');

Route::post('login','LoadController@authentication')->name('login_action');

Route::post('register','LoadController@registration');

Route::group(['namespace' => 'User','middleware' =>'user'], function () {

    Route::group(['prefix' => 'admin'],function(){
        Route::get('home','PageController@adminHome')->name('admin_home');
        Route::get('create_user','PageController@createUser')->name('create_user');
        Route::post('create_user','LoadController@createUser')->name('create_user_action');
        Route::get('all_users','PageController@allUsers')->name('all_users');
        Route::get('login_info/{id}','PageController@displayLoginInfo')->name('login_info');
        Route::get('change_status/{id}','PageController@changeUserStatus')->name('change_status');
    });

    Route::post('send','LoadController@sendMail')->name('send_email');

    Route::group(['prefix' => 'user'],function(){
        Route::get('home','PageController@home')->name('user_home');
    });

    Route::get('logout','LoadController@logout')->name('logout');
    Route::get('profile','PageController@profile')->name('profile_page');
    Route::post('update_profile','LoadController@updateProfile')->name('update_profile');
});
